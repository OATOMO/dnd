using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{

    private Unit unit;
    private enum State{
        WaitingForEnemyTurn,
        TakingTurn,
        Busy,
    }

    private State state;
    private float timer;
    private bool isInit = false;

    private void Awake(){
        state = State.WaitingForEnemyTurn;
        unit = GetComponentInParent<Unit>();
    }

    private void Start(){
        TurnSystem.Instance.OnTurnUnitChanged += TurnSystem_OnTurnUnitChanged;
    }

    // Update is called once per frame
    void Update()
    {
        if (TurnSystem.Instance.IsplayerTurn()){
            return;
        }

        if (!isInit)
        {
            if (!TurnSystem.Instance.IsplayerTurn())
            {
                state = State.TakingTurn;
                timer = 1f;
                isInit = true;
            }
        }
        
        switch (state){
            case State.WaitingForEnemyTurn:
                break;
            case State.TakingTurn:
                timer -= Time.deltaTime;
                if (timer <= 0f){
                    if ( TryTakeEnemyAIAction(SetStateTakingTurn)){
                        state = State.Busy;
                    }
                    else{
                        TurnSystem.Instance.NextUnit();
                    }

                }
                break;
            case State.Busy:
                break;
        }

    }

    private void SetStateTakingTurn(){
        timer = 0.5f;
        state = State.TakingTurn;
    }

    private void TurnSystem_OnTurnUnitChanged(object sender, EventArgs e){
        if (!TurnSystem.Instance.IsplayerTurn()){
            state = State.TakingTurn;
            timer = 1f;
        }
        // if (TurnSystem.Instance.IsMe(unit)){
        //     state = State.TakingTurn;
        //     timer = 2f;
        // }
    }

    private bool TryTakeEnemyAIAction(Action OnEnemyAIActionComplete){
        foreach (Unit enemyUnit in UnitManager.Instance.GetEnemyUnitList()){
            if (TurnSystem.Instance.IsMe(enemyUnit))
            {
                if (TryTakeEnemyAIAction(enemyUnit, OnEnemyAIActionComplete)){
                    return true;
                }
            }
            
        }

        return false;
    }

    private bool TryTakeEnemyAIAction(Unit enemyUnit,Action OnEnemyAIActionComplete){
        EnemyAIAction bestEnemyAIAction = null;
        BaseAction bestBaseAction = null;
        foreach (BaseAction baseAction in enemyUnit.GetBaseActionArray()){
            if (!enemyUnit.CanSpendActionPointsToTakeAction(baseAction)){
                continue;
            }

            if (bestEnemyAIAction == null){
                bestEnemyAIAction = baseAction.GetBestEnemyAIAction();
                bestBaseAction = baseAction;
            }
            else{
                EnemyAIAction testEnemyAIAction = baseAction.GetBestEnemyAIAction();
                if (testEnemyAIAction != null && testEnemyAIAction.actionValue > bestEnemyAIAction.actionValue){
                    //有更好的EnemyAIAction
                    bestEnemyAIAction = baseAction.GetBestEnemyAIAction();
                    bestBaseAction = baseAction;
                }
            }
        }

        if (bestEnemyAIAction != null && enemyUnit.trySpendActionPointsToTakeAction(bestBaseAction)){
            bestBaseAction.TakeAction(bestEnemyAIAction.gridPosition, OnEnemyAIActionComplete);
            return true;
        }
        else{
            return false;
        }
    }
}
