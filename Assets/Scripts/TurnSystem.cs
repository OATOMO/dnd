using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem.iOS;

public class TurnSystem : MonoBehaviour{
    public static TurnSystem Instance{ get; private set; }
    public event EventHandler OnTurnChanged;
    public event EventHandler OnTurnUnitChanged;
    public event EventHandler OnUpdateCurrentUnit;
    public event EventHandler OnLastUnit; //关卡一开始start全部unit时触发的事件
    private int turnNumber = 1;
    private List<Unit> haveDownList; //本回合行动过的unit
    private List<Unit> orderList; //按unit属性agility排序的全部unit
    private List<Unit> orderAllList; //按unit属性agility排序的剩下的unit
    private Unit currentUnit; //当前回合能行动的Unit
    private float timer;
    private const float TIMER = 0.01f;
    private bool initDown = false;
    
    private void Awake(){
        if (Instance!=null){
            Debug.LogError("There's more than one TurnSystem!!"+transform+"-"+Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
        haveDownList = new List<Unit>();
        orderList = new List<Unit>();
        UnitManager.Instance.OnUnitListChanged += UnitManager_OnUnitListChanged;
    }

    // private void Start()
    // {
    //     UnitManager.Instance.OnUnitListChanged += UnitManager_OnUnitListChanged;
    // }

    // public void NextTurn(){
    //     turnNumber++;
    //     isPlayerTurn = !isPlayerTurn;
    //     OnTurnChanged?.Invoke(this, EventArgs.Empty);//use:UnitActionSystemUI,EnemyAI,TurnSystemUI,Unit
    // }

    private void Update()
    {
        timer -= Time.deltaTime;
        if (!initDown && timer <= 0)//定时结束后到执行一次
        {
            OnLastUnit?.Invoke(this, EventArgs.Empty);
            initDown = true;
        }
    }

    public void NextTurn(){
        turnNumber++;
        haveDownList.Clear();
        UpdateList();
        currentUnit = orderAllList.First();
        
        OnTurnChanged?.Invoke(this, EventArgs.Empty);//use:UnitActionSystemUI,EnemyAI,TurnSystemUI,Unit
    }

    //下一个unit
    public void NextUnit()
    {
        // haveDownList.Append(currentUnit);//append不直接加元素,而是返回一个新的枚举
        haveDownList.Add(currentUnit);
        UpdateList();
        OnTurnUnitChanged?.Invoke(this, EventArgs.Empty);
    }

    public Unit GetCurrentUnit()
    {
        return currentUnit;
    }

    ///判断是不是入参unit回合
    public bool IsMe(Unit unit)
    {
        if (currentUnit == unit)
        {
            return true;
        }

        return false;
    }
    

    public int GetTurnNumber(){
        return turnNumber;
    }
    
    public bool IsplayerTurn(){
        if (currentUnit.IsEnemy())
        {
            return false;
        }
        return true;
    }

    //只有UpdateList()中在调用
    private void UpdateCurrentUnit()
    {
        if (orderList.Count <= 0)
        {
            print("NextTurn");
            NextTurn();
            return;
        }
        currentUnit = orderList.First();
        OnUpdateCurrentUnit?.Invoke(this, EventArgs.Empty);
    }

    private void UpdateList()
    {
        timer = TIMER;
        orderAllList = UnitManager.Instance.GetUnitList();
        orderAllList.Sort((Unit a, Unit b) => {return a.agility>b.agility ? -1 : 1;}); //全部的unit
        orderList.Clear();
        foreach (var VARIABLE in orderAllList)
        {
            orderList.Add(VARIABLE);
        }
        
        
        foreach (Unit haveDownUnit in haveDownList)
        {
            if (orderList.Contains(haveDownUnit))
            {
                orderList.Remove(haveDownUnit);//未行动的unit
            }
        }

        UpdateCurrentUnit();
    }

    //unitlist有变化,更新haveDownList与current
    private void UnitManager_OnUnitListChanged(object sender, EventArgs e)
    {
        UpdateList();
    }

}
