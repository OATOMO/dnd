using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAction : BaseAction{
    public event EventHandler OnStartMoving;
    public event EventHandler OnStopMoving;
    [SerializeField] private int maxMoveDistance = 4;
    [SerializeField] private AnimationCurve arcYanimationCurve;
    private List<Vector3> positionList;
    private int currentPositionIndex;
    private Vector3 beforePosition;
    private bool isJump;
    private float totalDistance;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isActive){
            return;
        }
        
        
        Vector3 targetPosition = positionList[currentPositionIndex];
        Vector3 moveDirection = (targetPosition - transform.position);
        moveDirection.y = 0;//移动的方向(归一化)
        moveDirection = moveDirection.normalized;
        
        totalDistance = Vector3.Distance(GetVectorXZ(beforePosition), GetVectorXZ(targetPosition));
        isJump = VerticalManager.Instance.IsParallel(beforePosition, targetPosition)? false: true;

        

        float rotateSpeed = 30.0f;
        transform.forward = Vector3.Lerp(transform.forward, moveDirection, Time.deltaTime*rotateSpeed);
        
        
        
        
        float stoppingDistance = .1f;
        if (Vector3.Distance(this.transform.position, targetPosition) > stoppingDistance){ 
            if (isJump)
            {
                Vector3 positionXZ = transform.position;
                positionXZ.y = 0;
                Vector3 targetPositionXZ = targetPosition;
                targetPositionXZ.y = 0;
                float distance = Vector3.Distance(positionXZ, targetPositionXZ);
                float distanceNormalized = 1 - distance / totalDistance;
                float maxHeight = 2f;
                // float positionY = arcYanimationCurve.Evaluate(distanceNormalized) * maxHeight + distanceNormalized*1;
                float positionY = VerticalManager.Instance.GetVertical(beforePosition) + distanceNormalized*
                    (VerticalManager.Instance.GetVertical(targetPosition) - VerticalManager.Instance.GetVertical(beforePosition)) + 
                    arcYanimationCurve.Evaluate(distanceNormalized) * maxHeight;
                
                float moveSpeed = 4.0f;
                transform.position += moveDirection * moveSpeed * Time.deltaTime;
                transform.position = new Vector3(transform.position.x,positionY,transform.position.z);
            }
            else
            {
                float moveSpeed = 4.0f;
                transform.position += moveDirection * moveSpeed * Time.deltaTime;
            }
        }
        else
        {
            currentPositionIndex++;
            beforePosition = positionList[currentPositionIndex - 1];
            if (currentPositionIndex >= positionList.Count)
            {
                OnStopMoving?.Invoke(this, EventArgs.Empty);//use:UnitAnimator(停止动画)
                ActionComplete();
            }
            
        }
        
    }

    private Vector3 GetVectorXZ(Vector3 v)
    {
        Vector3 vector = v;
        vector.y = 0;
        return vector;
    }

    public override void TakeAction(GridPosition gridPosition, Action onActionComplete)
    {
        List<GridPosition> pathGridPositionList =  PathFinding.Instance.FindPath(
            unit.GetGridPosition(), gridPosition, out int pathLength);
        currentPositionIndex = 0;
        positionList = new List<Vector3>();
        beforePosition = transform.position;
        foreach (GridPosition pathGridPosition in pathGridPositionList)
        {
            positionList.Add(VerticalManager.Instance.GetWorldPositionAddVertical(pathGridPosition));
            // positionList.Add(LevelGrid.Instance.GetWordPosition(pathGridPosition));
        }
        
        OnStartMoving?.Invoke(this, EventArgs.Empty);//use:UnitAnimator(走路动画)
        ActionStart(onActionComplete);
    }

    // public bool IsValidActionGridPosition(GridPosition gridPosition){
    //     List<GridPosition> validGridPositionList = GetValidActionGridPositionList();
    //     return validGridPositionList.Contains(gridPosition);
    // }

    public override List<GridPosition> GetValidActionGridPositionList(){
        List<GridPosition> validGridPositionList = new List<GridPosition>();
        GridPosition unitGridPosition = unit.GetGridPosition();
        for (int x = -maxMoveDistance; x <= maxMoveDistance; x++){
            for (int z = -maxMoveDistance; z <= maxMoveDistance; z++){
                GridPosition offsetGridPosition = new GridPosition(x, z);
                GridPosition testGridPosition = unitGridPosition + offsetGridPosition;
                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)){
                    continue;
                }

                if (unitGridPosition==testGridPosition){
                    //unit已经在这里了
                    continue;
                }

                if (LevelGrid.Instance.HasAnyUnitGridPosition(testGridPosition)){
                    //被其他unit占用
                    continue;
                }
                if (!PathFinding.Instance.IsWalkableGridPosition(testGridPosition))
                {
                    //有障碍物
                    continue;
                }

                if (!PathFinding.Instance.HasPath(unitGridPosition, testGridPosition))
                {
                    //寻路不可达
                    continue;
                }

                int pathFindDistanceMultiplier = 14;
                if (PathFinding.Instance.GetPathLength(unitGridPosition, testGridPosition) >
                    maxMoveDistance * pathFindDistanceMultiplier)
                {
                    //太远了(否则能移动到墙对面)
                    continue;
                }

                validGridPositionList.Add(testGridPosition);
            }
        }
        
        return validGridPositionList;
    }

    public override string GetActionName(){
        return "Move";
    }
    
    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition){
        int targetCountAtGridPosition =  unit.GetAction<ShootAction>().GetTargetCountAtPosition(gridPosition);
        return new EnemyAIAction(){
            gridPosition = gridPosition,
            actionValue = targetCountAtGridPosition * 10,
        };
    }
}
