using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowShootAction : BaseAction
{
    public event EventHandler<OnShootEventArgs> OnShootArrow;
    public event EventHandler OnReadyShootArrow;
    [SerializeField] private LayerMask obstaclesLayerMask;
    private float stateTimer;
    private enum State{
        Aiming,
        Shooting,
        Cooloff,
    }
    
    public class OnShootEventArgs : EventArgs{
        public Unit targetUnit;
        public Unit shootingUnit;
    }
    
    private State state;
    private int maxArrowDistance = 7;
    private Unit targetUnit;
    private bool canShootArrow;
    private bool canReadyShootArrow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isActive){
            return;
        }

        stateTimer -= Time.deltaTime;
        switch (state){
            case State.Aiming:
                if (canReadyShootArrow)
                {
                    OnReadyShootArrow?.Invoke(this, EventArgs.Empty);
                    canReadyShootArrow = false;
                }
                Vector3 aimDir = targetUnit.GetWorldPosition()-unit.GetWorldPosition();
                aimDir.y = 0;
                float rotateSpeed = 1.0f;
                transform.forward = Vector3.Lerp(transform.forward, aimDir, Time.deltaTime*rotateSpeed);
                break;
            case State.Shooting:
                if (canShootArrow){
                    Shoot();
                    canShootArrow = false;
                }
                break;
            case State.Cooloff:
                break;
        }

        if (stateTimer < 0f){
            NextState();
        }
    }
    
    private void Shoot(){
        // OnAnyShoot?.Invoke(this,new OnShootEventArgs(){
        //     targetUnit = targetUnit,
        //     shootingUnit = unit
        // });//use:ScreenShakeActions
        OnShootArrow?.Invoke(this,new OnShootEventArgs(){
            targetUnit = targetUnit,
            shootingUnit = unit
        });//use:UnitAnimator(射击动画和产生子弹)
        // targetUnit.Damage(40, unit.GetGridPosition());
    }

    public override string GetActionName()
    {
        return "Arrow";
    }
    
    private void NextState(){
        switch (state){
            case State.Aiming:
                state = State.Shooting;
                float shootingStateTime = 0.1f;
                stateTimer = shootingStateTime;
                break;
            case State.Shooting:
                state = State.Cooloff;
                float cooloffStateTime = 0.5f;
                stateTimer = cooloffStateTime;
                break;
            case State.Cooloff:
                ActionComplete();
                break;
        }

    }

    
 
    
    
    public override void TakeAction(GridPosition gridPosition, Action onActionComplete)
    {
        targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(gridPosition);
        state = State.Aiming;
        float aimingStateTime = 1f;
        stateTimer = aimingStateTime;

        canShootArrow = true;
        canReadyShootArrow = true;
        ActionStart(onActionComplete);
    }
    
    public override List<GridPosition> GetValidActionGridPositionList(){
        GridPosition unitGridPosition = unit.GetGridPosition();
        return GetValidActionGridPositionList(unitGridPosition);
    }

    public List<GridPosition> GetValidActionGridPositionList(GridPosition unitGridPosition)
    {
        List<GridPosition> validGridPositionList = new List<GridPosition>();
        for (int x = -maxArrowDistance; x <= maxArrowDistance; x++){
            for (int z = -maxArrowDistance; z <= maxArrowDistance; z++){
                GridPosition offsetGridPosition = new GridPosition(x, z);
                GridPosition testGridPosition = unitGridPosition + offsetGridPosition;
                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)){
                    continue;
                }
                //十字型范围
                int testDistance = Mathf.Abs(x) + Mathf.Abs(z);
                if (testDistance> maxArrowDistance){
                    continue;
                }
               
                if (unitGridPosition==testGridPosition){
                    //不选中自己
                    continue;
                }

                if (!LevelGrid.Instance.HasAnyUnitGridPosition(testGridPosition)){
                    //有其它unit
                    continue;
                }

                Unit targetUnit = LevelGrid.Instance.GetUnitAtGridPosition(testGridPosition);
                if (targetUnit.IsEnemy() == unit.IsEnemy()){
                    //不射击同一方
                    continue;
                }

                Vector3 unitWorldPosition = LevelGrid.Instance.GetWordPosition(unitGridPosition);
                Vector3 shootDir = (targetUnit.GetWorldPosition() - unitWorldPosition).normalized;
                float unitShoulderHeight = 1.7f;
                if (Physics.Raycast(unitWorldPosition + Vector3.up * unitShoulderHeight,
                        shootDir,
                        Vector3.Distance(unitWorldPosition, targetUnit.GetWorldPosition()),
                        obstaclesLayerMask))
                {
                    //弹道被obstaclesLayerMask层的障碍物阻挡
                    continue;
                }
                
                validGridPositionList.Add(testGridPosition);
            }
        }
        
        return validGridPositionList;
    }

    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition)
    {
        throw new NotImplementedException();
    }
    
}
