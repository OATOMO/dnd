using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushAction : BaseAction
{
    private int maxPushDistance = 1;
    [SerializeField]private bool isCanBePushed = true;  //是否能被推
    private bool isBePushed = false;    //是否正在被推
    private float bePushedTimer;
    private Action OnBePushedComplete;
    private Vector3 targetPosition;
    private float stoppingDistance = .1f;
    
    public event EventHandler OnBePushed;
    public event EventHandler OnPushed;
    
    private void Update()
    {
        if (isBePushed)
        {
            
            if (bePushedTimer >= 0)
            {
                bePushedTimer -= Time.deltaTime;
            }
            else
            {
                Vector3 moveDirection = (targetPosition - transform.position).normalized;//移动的方向(归一化)
                if (Vector3.Distance(this.transform.position, targetPosition) > stoppingDistance)
                {
                    float rotateSpeed = 15.0f;
                    transform.forward = Vector3.Lerp(transform.forward, -moveDirection, Time.deltaTime*rotateSpeed);
                
                    float moveSpeed = 4.0f;
                    transform.position += moveDirection * moveSpeed * Time.deltaTime;
                }
                else
                {
                    isBePushed = false;
                    OnBePushedComplete();
                }
            }
           
            

            // bePushedTimer -= Time.deltaTime;
            // if (bePushedTimer <= 0)
            // {
            //     isBePushed = false;
            //     OnBePushedComplete();
            // }
        }
        
        if (!isActive)
        {
            return;
        }


    }
    
    public override void TakeAction(GridPosition gridPosition, Action onActionComplete)
    {
        Unit BePushedUnit = LevelGrid.Instance.GetUnitAtGridPosition(gridPosition);
        if (BePushedUnit.TryGetComponent<PushAction>(out PushAction pushAction))
        {
            OnPushed?.Invoke(this, EventArgs.Empty);//use:UnitAnimator
            pushAction.BePushed(onActionComplete, unit.GetGridPosition());
            
            Vector3 moveDirectionSelf = (BePushedUnit.transform.position - transform.position).normalized;//移动的方向(归一化)

            transform.forward = moveDirectionSelf;
            ActionStart(onActionComplete);
            return;
        }
        else
        {
            Debug.Log("对象不能被推");   
            return;
        }
    }
    
    public override string GetActionName()
    {
        return "push";
    }
    

    public override List<GridPosition> GetValidActionGridPositionList()
    {
        List<GridPosition> validGridPositionList = new List<GridPosition>();
        GridPosition unitGridPosition = unit.GetGridPosition();
        for (int x = -maxPushDistance; x <= maxPushDistance; x++){
            for (int z = -maxPushDistance; z <= maxPushDistance; z++){
                GridPosition offsetGridPosition = new GridPosition(x, z);
                GridPosition testGridPosition = unitGridPosition + offsetGridPosition;
                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)){
                    continue;
                }
                
                if (unitGridPosition==testGridPosition){
                    //不选中自己
                    continue;
                }
                
                if (!LevelGrid.Instance.HasAnyUnitGridPosition(testGridPosition)){
                    //没有unit的gridPosition全跳过
                    continue;
                }

                if (LevelGrid.Instance.GetUnitAtGridPosition(testGridPosition)
                        .TryGetComponent<PushAction>(out PushAction pushAction) && !pushAction.isCanBePushed)
                {
                    continue;
                }
                
                if (!IsCanPushGridPosition(unitGridPosition,testGridPosition))
                {
                    continue;
                }


                validGridPositionList.Add(testGridPosition);
            }
        }
        
        return validGridPositionList;
    }

    public override EnemyAIAction GetEnemyAIAction(GridPosition gridPosition)
    {
        return new EnemyAIAction()
        {
            gridPosition = gridPosition,
            actionValue = 0
        };
    }
    
    private void OnPushComplete()
    {
        ActionComplete();
    }

    ///检测被推到的位置是否可达
    private bool IsCanPushGridPosition(GridPosition pushGridPosition, GridPosition bepushedGridPosition)
    {
        GridPosition dir = (pushGridPosition - bepushedGridPosition);
        GridPosition targetGridPosition = bepushedGridPosition - dir;
        if (LevelGrid.Instance.HasAnyUnitGridPosition(targetGridPosition)){
            return false;
        }
        
        return PathFinding.Instance.IsWalkableGridPosition(targetGridPosition);
    }

    //被推的对象
    public void BePushed(Action OnBePushedComplete,GridPosition gridPosition)
    {
        GridPosition dirGridPosition = unit.GetGridPosition()-gridPosition;
        targetPosition = LevelGrid.Instance.GetWordPosition(dirGridPosition+unit.GetGridPosition());
        
        this.OnBePushedComplete = OnBePushedComplete;
        
        bePushedTimer = 0.25f;
        
        Debug.Log(unit.name + " BePushed");
        OnBePushed?.Invoke(this,EventArgs.Empty);//use:UnitAnimator
        isBePushed = true;
    }
}
