﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour{
    private const int ACTION_POINTS_MAX = 9;

    public static event EventHandler OnAnyActionPointsChanged;
    public static event EventHandler OnAnyUnitSpawned;
    public static event EventHandler OnAnyUnitDead;

    [SerializeField] private bool isEnemy;
    [SerializeField] public int agility=10;
    
    private GridPosition gridPosition;
    private HealthSystem healthSystem;

    private BaseAction[] baseActionArray;
    private int actionPoints = ACTION_POINTS_MAX;//行动点数
    private void Awake(){
        healthSystem = GetComponent<HealthSystem>();
        baseActionArray = GetComponents<BaseAction>();
    }

    private void Start(){
        gridPosition = LevelGrid.Instance.getGridPosition(transform.position);
        LevelGrid.Instance.AddUnitAtGridPosition(gridPosition, this);

        TurnSystem.Instance.OnTurnChanged += TurnSystem_OnTurnChanged;
        healthSystem.OnDead += HealthSystem_OnDead;
        
        OnAnyUnitSpawned?.Invoke(this,EventArgs.Empty);//use:UnitManager
    }

    private void Update() {
        

        GridPosition newGridPosition = LevelGrid.Instance.getGridPosition(transform.position);
        if (newGridPosition != gridPosition){
            //unit changed grid position;
            GridPosition oldGridPosition = gridPosition;
            gridPosition = newGridPosition;
            LevelGrid.Instance.UnitMoveGridPosition(this, oldGridPosition,newGridPosition);
        }
    }

    public T GetAction<T>() where T :BaseAction
    {
        foreach (BaseAction baseAction in baseActionArray)
        {
            if (baseAction is T)
            {
                return (T)baseAction;
            }
        }

        return null;
    }
    

    public GridPosition GetGridPosition(){
        return gridPosition;
    }

    public Vector3 GetWorldPosition(){
        return transform.position;
    }

    public BaseAction[] GetBaseActionArray(){
        return baseActionArray;
    }

    public bool trySpendActionPointsToTakeAction(BaseAction baseAction){
        if (CanSpendActionPointsToTakeAction(baseAction)){
            SpendActionPoints(baseAction.GetActionPointsCost());
            return true;
        }
        else{
            return false;
        }
    }

    public bool CanSpendActionPointsToTakeAction(BaseAction baseAction){
        if (actionPoints >= baseAction.GetActionPointsCost()){
            return true;
        }
        else{
            return false;
        }
    }

    private void SpendActionPoints(int amount){
        actionPoints -= amount;
        OnAnyActionPointsChanged?.Invoke(this,EventArgs.Empty);//use:UnitActionSystemUI,UnitWorldUI
    }

    public int GetActionPoints(){
        return actionPoints;
    }

    private void TurnSystem_OnTurnChanged(object sender, EventArgs e){
        // if ((IsEnemy() && !TurnSystem.Instance.IsplayerTurn()) || //敌人的敌人回合 
        //     (!IsEnemy() && TurnSystem.Instance.IsplayerTurn()) ){ //玩家的玩家回合
        //     actionPoints = ACTION_POINTS_MAX;
        //     OnAnyActionPointsChanged?.Invoke(this,EventArgs.Empty);//use:UnitActionSystemUI,UnitWorldUI
        // }
        
            actionPoints = ACTION_POINTS_MAX;
            OnAnyActionPointsChanged?.Invoke(this,EventArgs.Empty);//use:UnitActionSystemUI,UnitWorldUI
        
        
    }

    public bool IsEnemy(){
        return isEnemy;
    }

    public void Damage(int damageAmount,GridPosition originGridPosition, int force = 1)
    {
        Vector3 dir = (LevelGrid.Instance.GetWordPosition(originGridPosition) -
                       LevelGrid.Instance.GetWordPosition(gridPosition)).normalized;
        transform.forward = dir;
        healthSystem.Damage(damageAmount, force);
    }

    private void HealthSystem_OnDead(object sender, EventArgs e){
        LevelGrid.Instance.RemoveUnitAtGridPosition(gridPosition, this);
        Destroy(gameObject);
        OnAnyUnitDead?.Invoke(this,EventArgs.Empty);//use:UnitManager
    }

    public float GetHealthNormalized(){
        return healthSystem.GetHealthNormalized();
    }

    public HealthSystem GetHealthSystem()
    {
        return healthSystem;
    }
}
