using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridVertical : MonoBehaviour
{
    [SerializeField] private int height = 1;

    public int GetHeight()
    {
        return height;
    }

}
