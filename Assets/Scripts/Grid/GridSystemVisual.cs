using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class GridSystemVisual : MonoBehaviour{
    public static GridSystemVisual Instance{ get; private set; }
    private LineRenderer lineRenderer;

    [Serializable] //自定义类型在面板操作的话得加这个
    public struct  GridVisualTypeMaterial{
        public GridVisualType gridVisualType;
        public Material material;
    }
    public enum GridVisualType{
        White,
        Blue,
        Red,
        RedSoft,
        Yellow
    }
    
    [SerializeField] private Transform gridSystemVisualSinglePrefab;
    [SerializeField] private List<GridVisualTypeMaterial> gridVisualTypeMaterialList;
    private GridSystemVisualSingle[,] gridSystemVisualSingleArray;
    // Start is called before the first frame update
    private void Awake(){
        if (Instance!=null){
            Debug.LogError("There's more than one UnitActionSystem!!"+transform+"-"+Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;

        lineRenderer = GetComponent<LineRenderer>();
    }
    void Start(){
        gridSystemVisualSingleArray = new GridSystemVisualSingle[
            LevelGrid.Instance.GetWidth(),
            LevelGrid.Instance.GetHeight()];
        for (int x = 0; x < LevelGrid.Instance.GetWidth(); x++){
            for (int z = 0; z < LevelGrid.Instance.GetHeight(); z++){
                GridPosition gridPosition = new GridPosition(x, z);
                Transform gridSystemVisualSingleTransform =
                    Instantiate(gridSystemVisualSinglePrefab, VerticalManager.Instance.GetWorldPositionAddVertical(gridPosition),Quaternion.identity);
                    // Instantiate(gridSystemVisualSinglePrefab, LevelGrid.Instance.GetWordPosition(gridPosition),Quaternion.identity);
                gridSystemVisualSingleArray[x, z] =
                    gridSystemVisualSingleTransform.GetComponent<GridSystemVisualSingle>();
            }
        }

        UnitActionSystem.Instance.OnSelectedActionChanged += UnitActionSystem_OnSelectedActionChanged;
        LevelGrid.Instance.OnAnyUnitMovedGridPosition += LevelGrid_OnAnyUnitMovedGridPosition;
        // UpdateGridVisual();
    }

    private void Update()
    {
        BaseAction selectedAction = UnitActionSystem.Instance.GetSelectedAction();
        if (selectedAction)
        {
            switch (selectedAction)
            {
                case MoveAction moveAction:
                    if (moveAction.GetValidActionGridPositionList().Contains(LevelGrid.Instance.getGridPosition(MouseWorld.GetPosition())))
                    {
                        Unit selectedUnit = UnitActionSystem.Instance.GetSelectedUnit();
                        List<GridPosition> pathGridPositionList = PathFinding.Instance.FindPath(selectedUnit.GetGridPosition(),
                            LevelGrid.Instance.getGridPosition(MouseWorld.GetPosition()),
                            out int pathLength);
                    
                        List<Vector3> points = new List<Vector3>();
                        
                        foreach (GridPosition position in pathGridPositionList)
                        {
                            points.Add(LevelGrid.Instance.GetWordPosition(position)+new Vector3(0,0.1f,0));
                        }

                        lineRenderer.positionCount = pathGridPositionList.Count;
                        lineRenderer.SetPositions(points.ToArray());
                    }
                    else
                    {
                        lineRenderer.positionCount = 0;
                    }
                    
                    break;
            }
        }
    }


    public void HideAllGridPosition(){
        for (int x = 0; x < LevelGrid.Instance.GetWidth(); x++){
            for (int z = 0; z < LevelGrid.Instance.GetHeight(); z++){
                gridSystemVisualSingleArray[x, z].Hide();
            }
        }
    }

    private void ShowGridPositionRange(GridPosition gridPosition, int range, GridVisualType gridVisualType){
        List<GridPosition> gridPositionList = new List<GridPosition>();
        for (int x = -range; x <= range; x++){
            for (int z = -range; z <= range; z++){
                GridPosition testGridPosition = gridPosition + new GridPosition(x, z);
                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)){
                    continue;
                }

                int testDistance = Mathf.Abs(x) + Mathf.Abs(z);
                if (testDistance>range){
                    continue;
                }
                gridPositionList.Add(testGridPosition);
            }
        }
        ShowGridPositionList(gridPositionList, gridVisualType);
    }
    
    private void ShowGridPositionRangeSquare(GridPosition gridPosition, int range, GridVisualType gridVisualType){
        List<GridPosition> gridPositionList = new List<GridPosition>();
        for (int x = -range; x <= range; x++){
            for (int z = -range; z <= range; z++){
                GridPosition testGridPosition = gridPosition + new GridPosition(x, z);
                if (!LevelGrid.Instance.IsValidGridPosition(testGridPosition)){
                    continue;
                }
                
                gridPositionList.Add(testGridPosition);
            }
        }
        ShowGridPositionList(gridPositionList, gridVisualType);
    }

    public void ShowGridPositionList(List<GridPosition> gridPositionList, GridVisualType gridVisualType){
        foreach (var gridPosition in gridPositionList){
            gridSystemVisualSingleArray[gridPosition.x,gridPosition.z].Show(getGridVisualTypeMaterial(gridVisualType));
        }
    }

    private void UpdateGridVisual(){
        HideAllGridPosition();
        BaseAction selectedAction = UnitActionSystem.Instance.GetSelectedAction();
        Unit selectedUnit = UnitActionSystem.Instance.GetSelectedUnit();
        GridVisualType gridVisualType;
        switch (selectedAction){
            default:
            case MoveAction moveAction:
                gridVisualType = GridVisualType.White;
                break;
            case SpinAction spinAction:
                gridVisualType = GridVisualType.Blue;
                break;
            case ShootAction shootAction:
                gridVisualType = GridVisualType.Red;
                ShowGridPositionRange(selectedUnit.GetGridPosition(), shootAction.GetMaxShootDistance(), GridVisualType.RedSoft);
                break;
            case GrenadeAction grenadeAction:
                gridVisualType = GridVisualType.Yellow;
                break;
            case SwordAction swordAction:
                gridVisualType = GridVisualType.Red;
                ShowGridPositionRange(selectedUnit.GetGridPosition(), swordAction.GetMaxSwordDistance(), GridVisualType.RedSoft);
                break;
            case InteractAction interactAction:
                gridVisualType = GridVisualType.Blue;
                break;
        }
        ShowGridPositionList(selectedAction.GetValidActionGridPositionList(),gridVisualType);
    }

    private void UnitActionSystem_OnSelectedActionChanged(object sender, EventArgs e){
        UpdateGridVisual();
    }
    
    private void LevelGrid_OnAnyUnitMovedGridPosition(object sender, EventArgs e){
        UpdateGridVisual();
    }

    private Material getGridVisualTypeMaterial(GridVisualType gridVisualType){
        foreach (GridVisualTypeMaterial gridVisualTypeMaterial in gridVisualTypeMaterialList){
            if (gridVisualTypeMaterial.gridVisualType == gridVisualType){
                return gridVisualTypeMaterial.material;
            }
        }

        Debug.Log("could not find GridVisualTypeMaterial for GridVisualType "+gridVisualType);
        return null;
    }
}
