using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * A*算法
 * f(n) = g(n)+h(n);
 * f(n)是节点n的综合优先级。当我们选择下一个要遍历的节点时，我们总会选取综合优先级最高（值最小）的节点。
 * g(n) 是节点n距离起点的代价。
 * h(n)是节点n距离终点的预计代价，这也就是A*算法的启发函数。
 * 解析:https://zhuanlan.zhihu.com/p/54510444
 * 论文:http://theory.stanford.edu/~amitp/GameProgramming/AStarComparison.html
 */
public class PathFinding : MonoBehaviour
{
    public static PathFinding Instance{ get; private set; }
    
    private const int MOVE_STRAIGHT_COST = 10;//直线成本
    private const int MOVE_DIAGONAL_COST = 14;//对角线成本 (ps:14的原因:1+1的平方根等于1.4)
    [SerializeField] private Transform gridDebugObjectPrefab;
    [SerializeField] private LayerMask obstaclesLayerMask;
    
    private int width;
    private int height;
    private float cellSize;
    private GridSystem<PathNode> gridSystem;

    private void Awake()
    {
        if (Instance!=null){
            Debug.LogError("There's more than one PathFinding!!"+transform+"-"+Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this; 
        
    }

    public void Setup(int width, int height, float cellSize)
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        
        gridSystem = new GridSystem<PathNode>(width, height, cellSize,
            (GridSystem<PathNode> g, GridPosition gridPosition) => new PathNode(gridPosition));
        // gridSystem.CreateDebugObjects(gridDebugObjectPrefab);
        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < height; z++)
            {
                GridPosition gridPosition = new GridPosition(x, z);
                Vector3 worldPosition = LevelGrid.Instance.GetWordPosition(gridPosition);
                float raycastOffsetDistance = 5f; //从碰撞器外部发射
                if (Physics.Raycast(worldPosition + Vector3.down * raycastOffsetDistance,
                        Vector3.up,
                        raycastOffsetDistance * 2,obstaclesLayerMask))
                {
                    GetNode(x,z).SetIsWalkable(false);
                }
            }
        }
    }
    ///寻路,返回路径列表
    public List<GridPosition> FindPath(GridPosition startGridPosition, GridPosition endGridPosition, out int pathLength)
    {
        List<PathNode> openList = new List<PathNode>();
        List<PathNode> closeList = new List<PathNode>();

        PathNode startNode = gridSystem.GetGridObject(startGridPosition);
        PathNode endNode = gridSystem.GetGridObject(endGridPosition);
        openList.Add(startNode);
        //初始化所有PathNode
        for (int x = 0; x < gridSystem.GetWidth(); x++)
        {
            for (int z = 0; z < gridSystem.GetHeight(); z++)
            {
                GridPosition gridPosition = new GridPosition(x, z);
                PathNode pathNode = gridSystem.GetGridObject(gridPosition);

                pathNode.SetGCost(int.MaxValue);
                pathNode.SetHCost(0);
                pathNode.CalculateFCost();
                pathNode.ResetCameFromPathNode();
            }
        }
        
        startNode.SetGCost(0);
        startNode.SetHCost(CalculateDistance(startGridPosition, endGridPosition));
        startNode.CalculateFCost();

        while (openList.Count > 0)
        {
            PathNode currentNode = GetLowestFCostPathNode(openList);
            if (currentNode == endNode)
            {
                pathLength = endNode.GetFCost();
                return CalculatePath(endNode);
            }

            openList.Remove(currentNode);
            closeList.Add(currentNode);

            foreach (PathNode neighbourNode in GetNeighbourList(currentNode))
            {
                if (closeList.Contains(neighbourNode))
                {
                    continue;                    
                }

                if (!neighbourNode.IsWalkable())
                {
                    closeList.Add(neighbourNode);
                    continue;//障碍物
                }

                //todo:暂定大于2不能跳跃,之后改为根据角色能力
                if (VerticalManager.Instance.GetVerticalSub(neighbourNode.GetGridPosition(), currentNode.GetGridPosition()) >= 2)
                {
                    continue;
                }
                //GCost是要向后传递的
                int tentativeGCost = currentNode.GetGCost() +
                                     CalculateDistance(currentNode.GetGridPosition(),
                                         neighbourNode.GetGridPosition());
                int VertivalCost = 10;//垂直成本
                tentativeGCost += 
                    VerticalManager.Instance.GetVerticalSub(neighbourNode.GetGridPosition(),
                        currentNode.GetGridPosition()) * VertivalCost;

                if (tentativeGCost < neighbourNode.GetGCost())
                {
                    //找到一个好的进入该节点的父节点
                    neighbourNode.SetCameFromPathNode(currentNode);
                    neighbourNode.SetGCost(tentativeGCost);
                    neighbourNode.SetHCost(CalculateDistance(neighbourNode.GetGridPosition(), endGridPosition));
                    neighbourNode.CalculateFCost();
                    if (!openList.Contains(neighbourNode))
                    {
                        openList.Add(neighbourNode);
                    }
                }

            }
        }
        //No path find
        pathLength = 0;
        return null;
    }

    ///A*算法启发函数
    public int CalculateDistance(GridPosition gridPositionA, GridPosition gridPositionB)
    {
        GridPosition gridPositionDistance = gridPositionA - gridPositionB;
        int xDistance = Mathf.Abs(gridPositionDistance.x);
        int zDistance = Mathf.Abs(gridPositionDistance.z);
        int remaining = Mathf.Abs(xDistance - zDistance);
        return MOVE_DIAGONAL_COST * Mathf.Min(xDistance, zDistance) + remaining * MOVE_STRAIGHT_COST;
    }
    
    /// 找到成本最低的PathNode
    private PathNode GetLowestFCostPathNode(List<PathNode> pathNodeList)
    {
        PathNode lowestFCostPathNode = pathNodeList[0];
        for (int i = 0; i < pathNodeList.Count; i++)
        {
            if (pathNodeList[i].GetFCost() < lowestFCostPathNode.GetFCost())
            {
                lowestFCostPathNode = pathNodeList[i];
            }
        }

        return lowestFCostPathNode;
    }

    private PathNode GetNode(int x, int z)
    {
        return gridSystem.GetGridObject(new GridPosition(x, z));
    }
    
    ///返回周围8方向邻居
    private List<PathNode> GetNeighbourList(PathNode currentNode)
    {
        List<PathNode> neighbourList = new List<PathNode>();
        GridPosition gridPosition = currentNode.GetGridPosition();

        if (gridPosition.x -1 >= 0)
        {
            //left
            neighbourList.Add(GetNode(gridPosition.x - 1,gridPosition.z + 0));
            if (gridPosition.z - 1 >= 0)
            {
                //left down
                neighbourList.Add(GetNode(gridPosition.x - 1, gridPosition.z - 1));
            }

            if (gridPosition.z + 1 < gridSystem.GetHeight())
            {
                //left up
                neighbourList.Add(GetNode(gridPosition.x - 1, gridPosition.z + 1));
            }
        }

        if (gridPosition.x + 1 < gridSystem.GetWidth() )
        {
            //right
            neighbourList.Add(GetNode(gridPosition.x + 1,gridPosition.z + 0));
            if (gridPosition.z - 1 >= 0)
            {
                //right down
                neighbourList.Add(GetNode(gridPosition.x + 1,gridPosition.z - 1));
            }

            if (gridPosition.z + 1 < gridSystem.GetHeight())
            {
                //right up
                neighbourList.Add(GetNode(gridPosition.x + 1,gridPosition.z + 1));
            }
            
        }

        if (gridPosition.z - 1 >= 0)
        {
            //down
            neighbourList.Add(GetNode(gridPosition.x + 0, gridPosition.z - 1));
        }

        if (gridPosition.z + 1 < gridSystem.GetHeight())
        {
            //up
            neighbourList.Add(GetNode(gridPosition.x + 0, gridPosition.z + 1));
        }

        return neighbourList;
    }
    
    /// 找到endNode后,返回从起点到终点的gridPositionList
    private List<GridPosition> CalculatePath(PathNode endNode)
    {
        List<PathNode> pathNodeList = new List<PathNode>();
        pathNodeList.Add(endNode);
        PathNode currentNode = endNode;
        while (currentNode.GetCameFromPathNode() != null)
        {
            pathNodeList.Add(currentNode.GetCameFromPathNode());
            currentNode = currentNode.GetCameFromPathNode();
        }
        pathNodeList.Reverse();
        List<GridPosition> gridPositionList = new List<GridPosition>();
        foreach (PathNode pathNode in pathNodeList)
        {
            gridPositionList.Add(pathNode.GetGridPosition());
        }

        return gridPositionList;

    }

    public void SetIsWalkableGridPosition(GridPosition gridPosition, bool isWalkable)
    {
        gridSystem.GetGridObject(gridPosition).SetIsWalkable(isWalkable);
    }

    public bool IsWalkableGridPosition(GridPosition gridPosition)
    {
        return gridSystem.GetGridObject(gridPosition).IsWalkable();
    }

    public bool HasPath(GridPosition startGridPosition, GridPosition endGridPosition)
    {
        return FindPath(startGridPosition, endGridPosition, out int pathLength) != null;
    }

    public int GetPathLength(GridPosition startGridPosition, GridPosition endGridPosition)
    {
        FindPath(startGridPosition, endGridPosition, out int pathLength);
        return pathLength;
    }


}
