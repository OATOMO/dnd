using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour{
    [SerializeField] private TrailRenderer trailRenderer;
    [SerializeField] private Transform bulletHitVfxPrefab;
    private Vector3 targetPosition;
    public void Setup(Vector3 targetPosition){
        this.targetPosition = targetPosition;
    }

    // Update is called once per frame
    void Update(){
        Vector3 moveDir = (targetPosition - transform.position).normalized;

        float distanceBeforMoving = Vector3.Distance(transform.position, targetPosition);
        float moveSpeed = 200f;
        transform.position += moveDir * moveSpeed * Time.deltaTime;
        
        float distanceAfterMoving = Vector3.Distance(transform.position, targetPosition);

        //移动前距离小于移动后距离 意味着已经冲过目标(速度快的物体得这样判断)
        if (distanceBeforMoving < distanceAfterMoving){
            transform.position = targetPosition;
            trailRenderer.transform.parent = null;
            Destroy(gameObject);
            Instantiate(bulletHitVfxPrefab, targetPosition, Quaternion.identity);
        }
    }
}
