using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelScript_def : MonoBehaviour
{
    [SerializeField] private List<GameObject> hiderList1;

    [SerializeField] private Door door1;
    // Start is called before the first frame update
    void Start()
    {
        door1.OnDoorOpened += (object sender, EventArgs e) =>
        {
            SetActiveGameObjectList(hiderList1, false);
        };

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void SetActiveGameObjectList(List<GameObject> gameObjectList, bool isActive)
    {
        foreach (GameObject gameObject in gameObjectList)
        {
            gameObject.SetActive(isActive);
        }
    }

}
