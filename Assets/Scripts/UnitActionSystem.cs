using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitActionSystem : MonoBehaviour{
    public static UnitActionSystem Instance{ get; private set; }
    public event EventHandler OnSelectedUnitChanged;
    public event EventHandler OnSelectedActionChanged;
    public event EventHandler<bool> OnBusyChanged;
    public event EventHandler OnActionStarted;
    [SerializeField] private Unit selectedUnit;
    [SerializeField] private LayerMask unitLayerMask;

    private BaseAction selectedAction;
    private bool isBusy;
    private bool isTurn;
    private void Awake(){
        if (Instance!=null){
            Debug.LogError("There's more than one UnitActionSystem!!"+transform+"-"+Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Start()
    {
        TurnSystem.Instance.OnLastUnit += TurnSystem_OnLastUnit;
        TurnSystem.Instance.OnTurnUnitChanged += TurnSystem_OnTurnUnitChanged;
        // SetSelectedUnit(selectedUnit);
    }

    private void Update(){
        if (isBusy){
            return;
        }

        if (!TurnSystem.Instance.IsplayerTurn()){
            return; //敌人回合
        }

        if (EventSystem.current.IsPointerOverGameObject()){//鼠标在UI上 
            return;
        }
        if (TryHandleUnitSelection()){
            return;
        } //选中帧的话只做一个动作,否则选中时会因为鼠标位置继续移动
        if (!isTurn)
        {
            return;
        }
        HandleSelectedAction();
    }

    private void TurnSystem_OnLastUnit(object sender, EventArgs e)
    {
        // print("TurnSystem_OnLastUnit");
        SetSelectedUnit(TurnSystem.Instance.GetCurrentUnit());
    }

    private void HandleSelectedAction(){
        if (InputManager.Instance.IsMouseButtonDownThisFrame()){
            GridPosition mouseGridPosition = LevelGrid.Instance.getGridPosition(MouseWorld.GetPosition());
            if (!selectedAction.IsValidActionGridPosition(mouseGridPosition)){
                return; //无效位置
            }

            if (!selectedUnit.trySpendActionPointsToTakeAction(selectedAction)){//这里尝试扣除行动点
                return; //无行动点数
            }

            SetBusy();
            selectedAction.TakeAction(mouseGridPosition, ClearBusy);
            OnActionStarted?.Invoke(this, EventArgs.Empty);//use:UnitActionSystemUI(更新行动点)
            // switch (selectedAction){
            //     case MoveAction moveAction:
            //         if (moveAction.IsValidActionGridPosition(mouseGridPosition)){
            //             SetBusy();
            //             moveAction.Move(mouseGridPosition,ClearBusy);
            //         }
            //         break;
            //     case SpinAction spinAction:
            //         SetBusy();
            //         spinAction.Spin(ClearBusy);
            //         break;
            // }

            
        }
    }

    private void SetBusy(){
        isBusy = true;
        OnBusyChanged?.Invoke(this, isBusy);//use:ActionBusyUI
    }

    private void ClearBusy(){
        isBusy = false;
        OnBusyChanged?.Invoke(this, isBusy);//use:ActionBusyUI
        if (selectedUnit.GetActionPoints() <= 0)
        {
            TurnSystem.Instance.NextUnit();
        }
    }

    public bool GetIsTurn()
    {
        return isTurn;
    }

    private bool TryHandleUnitSelection(){
        if (InputManager.Instance.IsMouseButtonDownThisFrame()){
            Ray ray = Camera.main.ScreenPointToRay(InputManager.Instance.GetMouseScreenPosition());
            if (Physics.Raycast(ray, out RaycastHit raycastHit, float.MaxValue, unitLayerMask)){
                if (raycastHit.transform.TryGetComponent<Unit>(out Unit unit)){
                    if (unit == selectedUnit){
                        return false;
                    }

                    if (unit.IsEnemy()){
                        return false;//不能选则敌人
                    }
                    SetSelectedUnit(unit);
                    return true;
                }
            }
        }
        return false;
    }

    private void SetSelectedUnit(Unit unit){
        selectedUnit = unit;
        SetSelectedAction(unit.GetAction<MoveAction>(), true);
        isTurn = TurnSystem.Instance.IsMe(selectedUnit) ? true : false;
        if (!isTurn)
        {
            print("不是我的回合");
        }
        OnSelectedUnitChanged?.Invoke(this,EventArgs.Empty);//use:UnitSelectedVisual,UnitActionSystemUI; 事件没有订阅者的话会引发异常
        // if (OnSelectedUnitChanged != null){
        //     OnSelectedUnitChanged(this,EventArgs.Empty);
        // }
    }

    public void SetSelectedAction(BaseAction baseAction, bool isInit = false){
        if (!isTurn && !isInit)
        {
            return;
        }
        selectedAction = baseAction;
        OnSelectedActionChanged?.Invoke(this,EventArgs.Empty);//use:UnitActionSystemUI,GridSystemVisual
    }

    public Unit GetSelectedUnit(){
        return selectedUnit;
    }

    public BaseAction GetSelectedAction(){
        return selectedAction;
    }

    public void TurnSystem_OnTurnUnitChanged(object sender, EventArgs e)
    {
        SetSelectedUnit(TurnSystem.Instance.GetCurrentUnit());
    }
}
