using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mono.Cecil.Cil;
using UnityEngine;

public class VerticalManager : MonoBehaviour
{
    public static VerticalManager Instance{ get; private set; }
    private GridVertical[] gridVerticals;
    private Dictionary<GridPosition, int> gridVerticalDict;

    private void Awake()
    {
        if (Instance!=null){
            Debug.LogError("There's more than one VerticalManager!!"+transform+"-"+Instance);
            Destroy(gameObject);
            return;
        }
        Instance = this;
        gridVerticalDict = new Dictionary<GridPosition, int>();
        
        gridVerticals = GameObject.FindObjectsOfType<GridVertical>();
        foreach (GridVertical gridVertical in gridVerticals)
        {
            gridVerticalDict.Add(LevelGrid.Instance.getGridPosition(gridVertical.transform.position),gridVertical.GetHeight());
        }
        
    }

    public int GetVertical(GridPosition gridPosition)
    {
        if (gridVerticalDict.ContainsKey(gridPosition))
        {
            return gridVerticalDict[gridPosition];
        }

        return 0;
    }
    public int GetVertical(Vector3 position)
    {
        return GetVertical(LevelGrid.Instance.getGridPosition(position));
    }

    public Vector3 GetWorldPositionAddVertical(GridPosition gridPosition)
    {
        if (gridVerticalDict.ContainsKey(gridPosition))
        {
            return LevelGrid.Instance.GetWordPosition(gridPosition) + Vector3.up * gridVerticalDict[gridPosition];
        }

        return LevelGrid.Instance.GetWordPosition(gridPosition);
    }

    ///return a-b
    public int GetVerticalSub(GridPosition gridPositiona, GridPosition gridPositionb)
    {
        return GetVertical(gridPositiona) - GetVertical(gridPositionb);
    }


    ///两个点是否平行,无高低差返回true
    public bool IsParallel(GridPosition gridPositiona,GridPosition gridPositionb)
    {
        return GetVertical(gridPositiona) == GetVertical(gridPositionb);
    }
    
    ///两个点是否平行,无高低差返回true
    public bool IsParallel(Vector3 positiona,Vector3 positionb)
    {
        return IsParallel(LevelGrid.Instance.getGridPosition(positiona),LevelGrid.Instance.getGridPosition(positionb));
    }
}
