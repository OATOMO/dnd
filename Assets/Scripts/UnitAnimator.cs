using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnimator : MonoBehaviour{
    [SerializeField] private Animator animator;
    [SerializeField] private Transform bulletProjectilePrefab;
    [SerializeField] private Transform shootPointTransform;
    
    [SerializeField] private Transform arrowProjectilePrefab;
    [SerializeField] private Transform shootArrowPointTransform;

    [SerializeField] private Transform rifleTransform;
    [SerializeField] private Transform swordTransform;
    // Start is called before the first frame update
    private void Awake(){
        if (TryGetComponent<MoveAction>(out MoveAction moveAction)){
            moveAction.OnStartMoving += moveAction_OnStartMoving;
            moveAction.OnStopMoving += moveAction_OnStopMoving;
        }
        if (TryGetComponent<ShootAction>(out ShootAction shootAction)){
            shootAction.OnShoot += shootAction_OnShoot;
        }
        if (TryGetComponent<SwordAction>(out SwordAction swordAction))
        {
            swordAction.OnSwordActionStarted += swordAction_OnSwordActionStarted;
            swordAction.OnSwordActionCompleted += swordAction_OnSwordActionCompleted;
        }

        if (TryGetComponent<PushAction>(out PushAction pushAction))
        {
            pushAction.OnPushed += pushAction_OnPushed;
            pushAction.OnBePushed += pushAction_OnBePushed;
        }
        if (TryGetComponent<InteractAction>(out InteractAction interactAction))
        {
            interactAction.OnInteractable += interactAction_OnInteractable;
        }
        if (TryGetComponent<BowShootAction>(out BowShootAction bowShootAction))
        {
            bowShootAction.OnShootArrow += bowShootAction_OnShootArrow;
            bowShootAction.OnReadyShootArrow += bowShootAction_OnReadyShootArrow;
        }

        HealthSystem healthSystem = GetComponent<HealthSystem>();
        healthSystem.OnDamaged += HealthSystem_OnDamaged;

    }

    private void Start()
    {
        equipRifle();
    }

    private void swordAction_OnSwordActionCompleted(object sender, EventArgs e)
    {
        equipRifle();
    }

    private void swordAction_OnSwordActionStarted(object sender, EventArgs e){
        EquipSword();
        animator.SetTrigger("SwordSlash");
    }

    private void moveAction_OnStartMoving(object sender, EventArgs e){
        animator.SetBool("IsWalking", true);
    }
    private void moveAction_OnStopMoving(object sender, EventArgs e){
        animator.SetBool("IsWalking", false);
    }


    private void bowShootAction_OnReadyShootArrow(object sender, EventArgs e)
    {
        animator.SetTrigger("ReadyShootArrow");
    }

    private void bowShootAction_OnShootArrow(object sender, BowShootAction.OnShootEventArgs e)
    {
        print(e.targetUnit.transform.position);
        animator.SetTrigger("ShootArrow");
        Transform arrowProjectileTransform =
            Instantiate(arrowProjectilePrefab, shootArrowPointTransform.position, Quaternion.identity);
        ArrowProjectile arrowProjectile = arrowProjectileTransform.GetComponent<ArrowProjectile>();
        Vector3 targetUnitShootAtPosition = e.targetUnit.GetWorldPosition();
        // targetUnitShootAtPosition.y = shootPointTransform.position.y;
        targetUnitShootAtPosition.y += 1.0f;
        arrowProjectile.Setup(targetUnitShootAtPosition);
        
    }

    private void shootAction_OnShoot(object sneder, ShootAction.OnShootEventArgs e){
        animator.SetTrigger("Shoot");
        Transform bulletProjectileTransform = Instantiate(bulletProjectilePrefab, shootPointTransform.position, Quaternion.identity);
        BulletProjectile bulletProjectile = bulletProjectileTransform.GetComponent<BulletProjectile>();

        Vector3 targetUnitShootAtPosition = e.targetUnit.GetWorldPosition();
        // targetUnitShootAtPosition.y = shootPointTransform.position.y;
        targetUnitShootAtPosition.y += 1.0f;
        // print(targetUnitShootAtPosition);
        bulletProjectile.Setup(targetUnitShootAtPosition);
    }
    
    private void pushAction_OnPushed(object sender, EventArgs e) {
        animator.SetTrigger("Pushed");
    }
    
    private void pushAction_OnBePushed(object sender, EventArgs e) {
        animator.SetTrigger("BePushed");
    }
    
    private void interactAction_OnInteractable(object sender, EventArgs e) {
        animator.SetTrigger("Interactable");
    }
    
    private void HealthSystem_OnDamaged(object sender, EventArgs e) {
        animator.SetTrigger("Damaged");
    }

    private void EquipSword()
    {
        swordTransform.gameObject.SetActive(true);
        rifleTransform.gameObject.SetActive(false);
    }

    private void equipRifle()
    {
        swordTransform.gameObject.SetActive(false);
        rifleTransform.gameObject.SetActive(true);
    }
    
}
