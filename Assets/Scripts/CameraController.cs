using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

//虚拟相机follow这个组件,look at这个组件
public class CameraController : MonoBehaviour{
    private const float MIN_FOLLOW_Y_OFFSET = 2f;
    private const float MAX_FOLLOW_Y_OFFSET = 12f;
    [SerializeField] private CinemachineVirtualCamera cinemachineVirtualCamera;
    private Vector3 targetFollowset;
    private CinemachineTransposer cinemachineTransposer;
    private Vector3 targetPosition;
    private bool isFollowDwon = true;
    
    // Start is called before the first frame update
    void Start()
    {
        cinemachineTransposer = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>();
        targetFollowset = cinemachineTransposer.m_FollowOffset;
        TurnSystem.Instance.OnTurnUnitChanged += TurnSystem_OnTurnUnitChanged;
        TurnSystem.Instance.OnLastUnit += TurnSystem_OnLastUnit;
    }

    // Update is called once per frame
    void Update(){
        HandleMovement();
        HandleRotation();
        HandleZoom();
        if (!isFollowDwon)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, 2.0f * Time.deltaTime);
            if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
            {
                isFollowDwon = true;
            }
        }
    }

    private void HandleMovement()
    {
        Vector2 inputMoveDir = InputManager.Instance.GetCameraMoveVector();
        if (inputMoveDir.magnitude > 0.5)
        {
            isFollowDwon = true;
        }
        


        float moveSpeed = 5f;
        Vector3 moveVector = transform.forward * inputMoveDir.y + transform.right * inputMoveDir.x;
        transform.position += moveVector * moveSpeed * Time.deltaTime;
    }

    private void HandleRotation(){
        Vector3 rotationVector = new Vector3(0, 0, 0);
        rotationVector.y += InputManager.Instance.GetCameraRotateAmount();

        float rotationSpeed = 100f;
        transform.eulerAngles += rotationVector * rotationSpeed * Time.deltaTime;
    }

    private void HandleZoom(){
        float zoomIncreaseAmount = 1f;
        targetFollowset.y += InputManager.Instance.GetCameraZoomAmount() * zoomIncreaseAmount;

        targetFollowset.y = Mathf.Clamp(targetFollowset.y, MIN_FOLLOW_Y_OFFSET, MAX_FOLLOW_Y_OFFSET);
        float zoomSpeed = 5f;
        cinemachineTransposer.m_FollowOffset =
            Vector3.Lerp(cinemachineTransposer.m_FollowOffset, targetFollowset, zoomSpeed*Time.deltaTime);
    }

    private void TurnSystem_OnTurnUnitChanged(object sender, EventArgs e)
    {
        Vector3 pos = TurnSystem.Instance.GetCurrentUnit().transform.position;
        targetPosition = new Vector3(pos.x, 0, pos.z);
        isFollowDwon = false;
    }
    
    private void TurnSystem_OnLastUnit(object sender, EventArgs e)
    {
        Vector3 pos = TurnSystem.Instance.GetCurrentUnit().transform.position;
        targetPosition = new Vector3(pos.x, 0, pos.z);
        isFollowDwon = false;
    }
}
