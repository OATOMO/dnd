using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ArrowProjectile : MonoBehaviour
{
    [SerializeField] private Transform arrowVFXPrefab;
    [SerializeField] private TrailRenderer trailRenderer;
    [SerializeField] private AnimationCurve arcYanimationCurve;

    private Vector3 targetPosition;

    private Vector3 originPosition;
    private float totalDistance;
    private bool isAction = false;
    // Start is called before the first frame update
    void Start()
    {
        originPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAction)
        {
            return;
        }

        transform.forward = (targetPosition - originPosition);
        totalDistance = Vector3.Distance(GetVectorXZ(originPosition), GetVectorXZ(targetPosition));
        
        float distance = Vector3.Distance(GetVectorXZ(transform.position), GetVectorXZ(targetPosition));
        float distanceNormalized = 1 - distance / totalDistance;
        float maxHeight = 2f;
        float positionY = originPosition.y + distanceNormalized*
                          (targetPosition.y - originPosition.y) + 
                          arcYanimationCurve.Evaluate(distanceNormalized) * maxHeight;
        
        
        Vector3 moveDir = (targetPosition - transform.position).normalized;

        float distanceBeforMoving = Vector3.Distance(transform.position, targetPosition);
        float moveSpeed = 10f;
        transform.position += moveDir * moveSpeed * Time.deltaTime;
        transform.position = new Vector3(transform.position.x, positionY, transform.position.z);
        
        float distanceAfterMoving = Vector3.Distance(transform.position, targetPosition);

        //移动前距离小于移动后距离 意味着已经冲过目标(速度快的物体得这样判断)
        if (distanceBeforMoving < distanceAfterMoving){
            transform.position = targetPosition;
            trailRenderer.transform.parent = null;
            Destroy(gameObject);
            Instantiate(arrowVFXPrefab, targetPosition, Quaternion.identity);
        }
    }

    public void Setup(Vector3 targetPosition)
    {
        this.targetPosition = targetPosition;
        print(targetPosition);
        isAction = true;
    }
    
    private Vector3 GetVectorXZ(Vector3 v)
    {
        Vector3 vector = v;
        vector.y = 0;
        return vector;
    }
}
