using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapSpears : MonoBehaviour
{
    private Animator animator;
    private bool turnTrigger = true;   //这回合能否触发
    private GridPosition gridPosition;
    private Unit unit;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        gridPosition = LevelGrid.Instance.getGridPosition(transform.position);
    }

    // Start is called before the first frame update
    void Start()
    {
        LevelGrid.Instance.OnUnitMovedGridPosition+= LevelGrid_OnUnitMovedGridPosition;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LevelGrid_OnUnitMovedGridPosition(object sender, Unit unit)
    {
        if (turnTrigger)
        {
            if (unit.GetGridPosition() == gridPosition)
            {
                this.unit = unit;
                switch (UnitActionSystem.Instance.GetSelectedAction())
                {
                    case  MoveAction moveAction:
                        nowTrigger();
                        break;
                    default:
                        StartCoroutine("delayedTrigger");
                        break;
                }
            }
            
        }
    }

    IEnumerator delayedTrigger()
    {
        yield return new WaitForSeconds(0.7f);
        //进入触发式陷阱
        unit.Damage(30, unit.GetGridPosition());
        animator.SetTrigger("Trigger");
        // turnTrigger = false;
    }

    private void nowTrigger()
    {
        unit.Damage(30, unit.GetGridPosition());
        animator.SetTrigger("Trigger");
    }


}
