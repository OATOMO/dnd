using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitRagdoll : MonoBehaviour{
    [SerializeField] private Transform ragdollRootBone;

    public void Setup(Transform originalRootBone){
        MatchAllChildTransforms(originalRootBone, ragdollRootBone);

        Vector3 randomDir = new Vector3(Random.Range(-1f, +1f), 0, Random.Range(-1f, +1f));
        ApplyExplosionToRagdoll(ragdollRootBone,300f,transform.position +randomDir,10f);
    }
    
    //为布娃娃拷贝原来动作的rootBone
    private void MatchAllChildTransforms(Transform root, Transform clone){
        foreach (Transform child in root){
            Transform cloneChild = clone.Find(child.name);
            if (cloneChild != null){
                cloneChild.position = child.position;
                cloneChild.rotation = child.rotation;
                
                MatchAllChildTransforms(child, cloneChild);
            }
        }
    }

    //给ragdoll施加一个力(爆炸效果)
    private void ApplyExplosionToRagdoll(Transform root, float explosionForce, Vector3 explosionPosition, float explosionRange){
        foreach (Transform child in root){
            if (child.TryGetComponent<Rigidbody>(out Rigidbody childRigidbody)){
                childRigidbody.AddExplosionForce(explosionForce, explosionPosition, explosionRange);
            }
            ApplyExplosionToRagdoll(child, explosionForce, explosionPosition, explosionRange);
        }
    }

}
