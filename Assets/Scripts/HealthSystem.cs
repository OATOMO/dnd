using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour{
    public event EventHandler OnDead;
    public event EventHandler OnDamaged;
    
    [SerializeField] private int health = 100;
    private int healthMax;

    private void Awake(){
        healthMax = health;
    }

    public void Damage(int damageAmount, int force){
        health -= damageAmount;
        if (health<0){
            health = 0;
        }
        
        OnDamaged?.Invoke(this,EventArgs.Empty);//use:UnitWorldUI,UnitAnimator

        if (health == 0){
            Die();
        }
    }

    private void Die(){
        OnDead?.Invoke(this, EventArgs.Empty);//use:Unit,UnitRagdollSpawner
    }

    public float GetHealthNormalized(){
        return (float)health / healthMax;
    }
}
