using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TurnSystemUI : MonoBehaviour{
    [SerializeField] private Button endTurnBtn;
    [SerializeField] private TextMeshProUGUI turnNumberText;
    [SerializeField] private GameObject enemyTurnVisualGameObject;

    private void Start(){
        endTurnBtn.onClick.AddListener(() => {
            TurnSystem.Instance.NextUnit();
        });
        TurnSystem.Instance.OnTurnChanged += TurnSystem_OnTurnChanged;
        TurnSystem.Instance.OnUpdateCurrentUnit += TurnSystem_OnUpdateCurrentUnit;
        UpdateTurnText();
        UpdateEnemyTurnVisual();
        UpdateEndTurnButtonVisibility();
    }

    private void TurnSystem_OnTurnChanged(object sender, EventArgs e){
        UpdateTurnText();
        UpdateEnemyTurnVisual();
        UpdateEndTurnButtonVisibility();
    }
    
    private void TurnSystem_OnUpdateCurrentUnit(object sender, EventArgs e){
        UpdateTurnText();
        UpdateEnemyTurnVisual();
        UpdateEndTurnButtonVisibility();
    }

    private void UpdateTurnText(){
        turnNumberText.text = "Turn " + TurnSystem.Instance.GetTurnNumber();
    }

    private void UpdateEnemyTurnVisual(){
        enemyTurnVisualGameObject.SetActive(!TurnSystem.Instance.IsplayerTurn());
    }

    private void UpdateEndTurnButtonVisibility(){
        endTurnBtn.gameObject.SetActive(TurnSystem.Instance.IsplayerTurn());
    }
}
